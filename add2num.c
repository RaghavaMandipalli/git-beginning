#include <stdio.h>

/* This program adds two numbers taken
 * from user and displays result back to
 * user
 */

int main(void)
{
    int num1 = 0;
    int num2 = 0;
    int sum = 0;
    
    printf("Enter num1\n");
    scanf("%d", &num1);

    printf("Enter num2\n");
    scanf("%d", &num2);
    
    sum = num1 + num2;
        
    printf("sum of given numbers %d and %d is %d\n", num1, num2, sum);
    
    return 0;
}
